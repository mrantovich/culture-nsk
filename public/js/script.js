$(document).ready(function(){

	$('.navbar__burger').on('click', function(){
		$('.navbar__list, .navbar__burger').toggleClass('opened');
	});

	$('.main__slider').slick({
		dots: true,
		infinite: true,
		speed: 700,
		slidesToShow: 1,
		fade: true,
		cssEase: 'linear',
		mobileFirst: true,
		autoplay: true,
		arrows: false,
		adaptiveHeight: true
	});

	$('.cards__item').addClass('cards__visible');

	$('.districts__item').on('click', function() {
		$('.districts__item').removeClass('districts__active');
		$(this).addClass('districts__active');
		$('.cards__item').removeClass('cards__visible');

		var district = $(this).attr('id');
		if (district == 'district-zhd') {
			$('div[name^="zhd-district"]').addClass('cards__visible');
		}
		else if (district == "district-central") {
			$('div[name^="central-district"]').addClass('cards__visible');
		}
		else if (district == "district-z") {
			$('div[name^="z-district"]').addClass('cards__visible');
		}
		else if (district == "district-all") {
			$('div[name^="zhd-district"]').addClass('cards__visible');
			$('div[name^="central-district"]').addClass('cards__visible');
			$('div[name^="z-district"]').addClass('cards__visible');
		};
	});

	$('#info-about').addClass('info-body_active');
	$('div[name=info-about').addClass('info-body__content_active');
	var firsth = $('div[name=info-about').height();
	$('.info-body__text').height(firsth);

	$(window).on('resize', function() {
		var cur = $('.info-body_active').attr('id');
		var h = $('div[name='+cur+']').height();
		$('.info-body__text').height(h);
	});

	$('.info-body ul li').on('click', function() {
		$('.info-body ul li').removeClass('info-body_active');
		$(this).addClass('info-body_active');
		$('.info-body__content').removeClass('info-body__content_active');

		var cur = $(this).attr('id');
		$('div[name='+cur+']').addClass('info-body__content_active');
		var h = $('div[name='+cur+']').height();
		$('.info-body__text').height(h);
	});

});